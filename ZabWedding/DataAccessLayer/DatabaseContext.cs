﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZabWedding.Models;

namespace ZabWedding.DataAccessLayer
{
    public class DatabaseContext : DbContext
    {

        // use to update database https://www.entityframeworktutorial.net/efcore/entity-framework-core-migration.aspx
        // command EntityFrameworkCore\Add-Migration and then EntityFrameworkCore\Update-database 
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        public DbSet<Billeder> Billeder { get; set; }
        public DbSet<Tilmelding> Tilmeldinger { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Billeder>().ToTable("Billeder");
            modelBuilder.Entity<Tilmelding>().ToTable("Tilmeldinger");
        }
    }
}
