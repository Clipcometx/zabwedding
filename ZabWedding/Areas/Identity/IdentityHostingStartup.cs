﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ZabWedding.Areas.Identity.Data;
using ZabWedding.Login;

[assembly: HostingStartup(typeof(ZabWedding.Areas.Identity.IdentityHostingStartup))]
namespace ZabWedding.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<ZabWeddingContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("ZabWeddingContextConnection")));

                services.AddDefaultIdentity<ZabWeddingUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<ZabWeddingContext>();
            });
        }
    }
}