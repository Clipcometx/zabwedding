﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZabWedding.Models
{
    public class Billeder
    {
        public int ID { get; set; }
        public string Billede { get; set; }
        public string Thumnail { get; set; }
        public bool Fotograf { get; set; }
        public bool MySelfie { get; set; }
    }
}
