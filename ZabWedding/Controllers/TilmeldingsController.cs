﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ZabWedding.DataAccessLayer;
using ZabWedding.Models;

namespace ZabWedding.Controllers
{
    public class TilmeldingsController : Controller
    {
        private readonly DatabaseContext _context;

        public TilmeldingsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: Tilmeldings
        public async Task<IActionResult> Index()
        {
            return View(await _context.Tilmeldinger.ToListAsync());
        }

        // GET: Tilmeldings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tilmelding = await _context.Tilmeldinger
                .FirstOrDefaultAsync(m => m.ID == id);
            if (tilmelding == null)
            {
                return NotFound();
            }

            return View(tilmelding);
        }

        // GET: Tilmeldings/Create
        public async Task<IActionResult> Create()
        {
            List<Tilmelding> test = await _context.Tilmeldinger.ToListAsync();
            ViewBag.peopleAmount = test.Count; 
            return View();
        }

        // POST: Tilmeldings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Fornavn,Efternavn,Allegi")] Tilmelding tilmelding)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tilmelding);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Create));
            }
            return View(tilmelding);
        }

        // GET: Tilmeldings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tilmelding = await _context.Tilmeldinger.FindAsync(id);
            if (tilmelding == null)
            {
                return NotFound();
            }
            return View(tilmelding);
        }

        // POST: Tilmeldings/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Fornavn,Efternavn,Allegi")] Tilmelding tilmelding)
        {
            if (id != tilmelding.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tilmelding);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TilmeldingExists(tilmelding.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tilmelding);
        }

        // GET: Tilmeldings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tilmelding = await _context.Tilmeldinger
                .FirstOrDefaultAsync(m => m.ID == id);
            if (tilmelding == null)
            {
                return NotFound();
            }

            return View(tilmelding);
        }

        // POST: Tilmeldings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tilmelding = await _context.Tilmeldinger.FindAsync(id);
            _context.Tilmeldinger.Remove(tilmelding);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TilmeldingExists(int id)
        {
            return _context.Tilmeldinger.Any(e => e.ID == id);
        }
    }
}
