﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SixLabors.ImageSharp.Formats.Jpeg;
using ZabWedding.DataAccessLayer;
using ZabWedding.Models;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Formats;
using System.Drawing;


namespace ZabWedding.Controllers
{
    public class BilledersController : Controller
    {
        private readonly DatabaseContext _context;

        public BilledersController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: Billeders
        /*
        public async Task<IActionResult> Index()
        {

            return View(await _context.Billeder.ToListAsync());
        }
        */

        public async Task<IActionResult> Index(bool myselfies, bool fotograf, bool resten)
        {


            if (fotograf == true && myselfies == true && resten == true)
            {
                return View(await _context.Billeder
                .Where(x => x.Fotograf == true || x.MySelfie == true || x.Fotograf == false && x.MySelfie == false) 
                .ToListAsync());
            }
            if (fotograf == true && myselfies == true && resten == false)
            {
                return View(await _context.Billeder
                .Where(x => x.Fotograf == true || x.MySelfie == true)
                .ToListAsync());
            }

            if (fotograf == false && myselfies == true && resten == true)
            {
                return View(await _context.Billeder
                .Where(x => x.Fotograf == false && x.MySelfie == true || x.Fotograf == false && x.MySelfie == false)
                .ToListAsync());
            }
            if (fotograf == false && myselfies == true && resten == false)
            {
                return View(await _context.Billeder
                .Where(x => x.Fotograf == false && x.MySelfie == true)
                .ToListAsync());
            }

            if (fotograf == true && myselfies == false && resten == true)
            {
                return View(await _context.Billeder
                .Where(x => x.Fotograf == true && x.MySelfie == false || x.Fotograf == false && x.MySelfie == false)
                .ToListAsync());
            }
            if (fotograf == true && myselfies == false && resten == false)
            {
                return View(await _context.Billeder
                .Where(x => x.Fotograf == true && x.MySelfie == false)
                .ToListAsync());
            }




            return View(await _context.Billeder.ToListAsync());

        }

        // GET: Billeders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billeder = await _context.Billeder
                .FirstOrDefaultAsync(m => m.ID == id);
            if (billeder == null)
            {
                return NotFound();
            }

            return View(billeder);
        }

        // GET: Billeders/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Billeders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Billeder weddingPictureInfo, List<IFormFile> files)
        {

            if (ModelState.IsValid)
            {
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        using (var ms = new MemoryStream())
                        {
                            file.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            weddingPictureInfo.Billede = Convert.ToBase64String(fileBytes);
                            // act on the Base64 data
                        }
                    }
                }

                _context.Add(weddingPictureInfo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(weddingPictureInfo);
        }

        // GET: Billeders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billeder = await _context.Billeder.FindAsync(id);
            if (billeder == null)
            {
                return NotFound();
            }
            return View(billeder);
        }

        // POST: Billeders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Billede,Fotograf,MySelfie")] Billeder billeder)
        {
            if (id != billeder.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(billeder);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BillederExists(billeder.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(billeder);
        }

        // GET: Billeders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billeder = await _context.Billeder
                .FirstOrDefaultAsync(m => m.ID == id);
            if (billeder == null)
            {
                return NotFound();
            }

            return View(billeder);
        }

        // POST: Billeders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var billeder = await _context.Billeder.FindAsync(id);
            _context.Billeder.Remove(billeder);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BillederExists(int id)
        {
            return _context.Billeder.Any(e => e.ID == id);
        }
    }
}
