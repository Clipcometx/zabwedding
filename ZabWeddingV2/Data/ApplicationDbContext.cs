﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ZabWeddingV2.Models;

namespace ZabWeddingV2.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        // use to update database https://www.entityframeworktutorial.net/efcore/entity-framework-core-migration.aspx
        // command EntityFrameworkCore\Add-Migration and then EntityFrameworkCore\Update-database
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<ZabWeddingV2.Models.Billeder> Billeder { get; set; }
        public DbSet<ZabWeddingV2.Models.Tilmelding> Tilmelding { get; set; }
    }
}
