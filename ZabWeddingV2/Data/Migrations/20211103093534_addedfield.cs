﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ZabWeddingV2.Data.Migrations
{
    public partial class addedfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Thumbnail",
                table: "Billeder",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Thumbnail",
                table: "Billeder");
        }
    }
}
