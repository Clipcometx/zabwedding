﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ZabWeddingV2.Data;
using ZabWeddingV2.Models;

namespace ZabWeddingV2.Controllers
{
    [Authorize(Roles = "Administrator,Manager,User")]
    public class TilmeldingsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TilmeldingsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Tilmeldings
        [Authorize(Roles = "Administrator,Manager")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Tilmelding.ToListAsync());
        }

        // GET: Tilmeldings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tilmelding = await _context.Tilmelding
                .FirstOrDefaultAsync(m => m.ID == id);
            if (tilmelding == null)
            {
                return NotFound();
            }

            return View(tilmelding);
        }

        // GET: Tilmeldings/Create
        [Authorize(Roles = "Administrator,Manager,User")]
        public async Task<IActionResult> Create()
        {
            var list = await _context.Tilmelding.ToListAsync();
            ViewBag.peopleAmount = list.Count;
            return View();
        }

        // POST: Tilmeldings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Manager,User")]
        public async Task<IActionResult> Create([Bind("ID,Fornavn,Efternavn,Allegi")] Tilmelding tilmelding)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tilmelding);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Create));
            }
            return View(tilmelding);
        }

        // GET: Tilmeldings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tilmelding = await _context.Tilmelding.FindAsync(id);
            if (tilmelding == null)
            {
                return NotFound();
            }
            return View(tilmelding);
        }

        // POST: Tilmeldings/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Fornavn,Efternavn,Allegi")] Tilmelding tilmelding)
        {
            if (id != tilmelding.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tilmelding);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TilmeldingExists(tilmelding.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tilmelding);
        }

        // GET: Tilmeldings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tilmelding = await _context.Tilmelding
                .FirstOrDefaultAsync(m => m.ID == id);
            if (tilmelding == null)
            {
                return NotFound();
            }

            return View(tilmelding);
        }

        // POST: Tilmeldings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tilmelding = await _context.Tilmelding.FindAsync(id);
            _context.Tilmelding.Remove(tilmelding);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TilmeldingExists(int id)
        {
            return _context.Tilmelding.Any(e => e.ID == id);
        }
    }
}
