﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ZabTheis.Data;
using ZabTheis.Models;

namespace ZabTheis
{
    public class BilledersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BilledersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Billeders
        public async Task<IActionResult> Index()
        {
            return View(await _context.Billeder.ToListAsync());
        }

        // GET: Billeders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billeder = await _context.Billeder
                .FirstOrDefaultAsync(m => m.ID == id);
            if (billeder == null)
            {
                return NotFound();
            }

            return View(billeder);
        }

        // GET: Billeders/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Billeders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Billede,Fotograf,MySelfie")] Billeder billeder)
        {
            if (ModelState.IsValid)
            {
                _context.Add(billeder);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(billeder);
        }

        // GET: Billeders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billeder = await _context.Billeder.FindAsync(id);
            if (billeder == null)
            {
                return NotFound();
            }
            return View(billeder);
        }

        // POST: Billeders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Billede,Fotograf,MySelfie")] Billeder billeder)
        {
            if (id != billeder.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(billeder);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BillederExists(billeder.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(billeder);
        }

        // GET: Billeders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billeder = await _context.Billeder
                .FirstOrDefaultAsync(m => m.ID == id);
            if (billeder == null)
            {
                return NotFound();
            }

            return View(billeder);
        }

        // POST: Billeders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var billeder = await _context.Billeder.FindAsync(id);
            _context.Billeder.Remove(billeder);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BillederExists(int id)
        {
            return _context.Billeder.Any(e => e.ID == id);
        }
    }
}
